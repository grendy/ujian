var express = require("express");
var router = express.Router();
const superadmin = require("../controller/SuperAdmin");
const { isLoginSuperAdmin } = require("../middleware/isLogin");

/* GET Admin home page. */
router.get("/", function (req, res, next) {
  res.redirect("/superadmin/login");
});
router.get("/home", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewHome();
});
router.get("/sekolah", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewSekolah();
});
router.post("/sekolah", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).addSekolah();
});
router.put("/sekolah/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).upSekolah();
});
router.delete("/sekolah/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).delSekolah();
});
router.post("/import-sekolah", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).importSekolah();
});
router.get("/export-sekolah", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).exportSekolah();
});
router.get("/form-sekolah/:id?", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewFormSekolah();
});
router.get("/sekolah/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewInfoSekolah();
});

router.get("/peserta", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewPeserta();
});
router.get("/edit-siswa", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).updatePeserta();
});

router.get("/bahan-ajar", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewBahanAjar();
});
router.get("/edit-bahan-ajar", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).updateBahanAjar();
});

router.get("/tim-teknis", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewTimTeknis();
});
router.post("/tim-teknis", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).addTimTeknis();
});
router.post("/import-tim-teknis", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).importTimTeknis();
});
router.put("/tim-teknis/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).upTimTeknis();
});
router.delete("/tim-teknis/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).delTimTeknis();
});
router.get(
  "/form-tim-teknis/:id?",
  isLoginSuperAdmin,
  function (req, res, next) {
    new superadmin(req, res).formTimTeknis();
  }
);
router.get("/format-tim-teknis", isLoginSuperAdmin, function (req, res, next) {
  const file = `./public/files/format/tim-teknis.xlsx`;
  return res.download(file);
});

router.get("/bank-soal", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewBankSoal();
});
router.get("/data-soal/:id_banksoal", isLoginSuperAdmin, function (req, res) {
  new superadmin(req, res).viewDataSoal();
});
router.get("/file-pendukung", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewFilePendukung();
});
router.get("/jadwal", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewJadwal();
});

router.get("/kartu-ujian", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewKartuUjian();
});
router.delete("/kartu-ujian/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).delKartuUjian();
});
router.get("/cetak-kartu/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewCetakKartu();
});

router.get("/berita-acara", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewBeritaAcara();
});
router.post("/berita-acara", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).addBeritaAcara();
});
router.delete("/berita-acara/:id", isLoginSuperAdmin, function (req, res) {
  new superadmin(req, res).delBeritaAcara();
});
router.get(
  "/download-berita-acara/:id",
  isLoginSuperAdmin,
  function (req, res) {
    new superadmin(req, res).downloadBeritaAcara();
  }
);

router.get("/daftar-hadir/:id?", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewDaftarHadir();
});
router.get("/cetak-daftar-hadir/:id", isLoginSuperAdmin, function (req, res) {
  new superadmin(req, res).viewCetakDaftarHadir();
});

router.get("/nilai-ujian", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewNilai();
});
router.get("/semua-nilai", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewSemuaNilai();
});
router.get("/detail-nilai", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewDetailNilai();
});

router.get("/pengumuman", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).viewPengumuman();
});
router.get(
  "/form-pengumuman/:id?",
  isLoginSuperAdmin,
  function (req, res, next) {
    new superadmin(req, res).viewFormPengumuman();
  }
);
router.post("/pengumuman", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).addPengumuman();
});
router.put("/pengumuman/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).upPengumuman();
});
router.delete("/pengumuman/:id", isLoginSuperAdmin, function (req, res, next) {
  new superadmin(req, res).delPengumuman();
});

router.get("/login", function (req, res, next) {
  new superadmin(req, res).viewLogin();
});
router.post("/login", function (req, res, next) {
  new superadmin(req, res).login();
});
router.get("/logout", function (req, res, next) {
  new superadmin(req, res).logout();
});

router.get("/format-sekolah", isLoginSuperAdmin, function (req, res, next) {
  const file = `./public/files/format/sekolah.xlsx`;
  return res.download(file);
});
module.exports = router;
