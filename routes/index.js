var express = require("express");
var router = express.Router();
const index = require("../controller/Index");
const { isLoginSiswa } = require("../middleware/isLogin");

router.get("/", function (req, res, next) {
  res.redirect("/login");
});
router.get("/login", function (req, res, next) {
  new index(req, res).viewLogin();
});
router.post("/login", function (req, res, next) {
  new index(req, res).login();
});
router.get("/home", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewHome();
});
router.get("/jadwal-ujian", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewJadwalUjian();
});
router.get("/mulai-ujian/:id", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewKonfirmUjian();
});
router.get("/mulai/:id", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewUjian();
});
router.post("/selesai/:id", isLoginSiswa, function (req, res, next) {
  new index(req, res).selesaiUjian();
});
router.get("/bahan-ajar", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewBahanAjar();
});
router.get("/lihat-materi/:id", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewMateri();
});
router.get("/hasil-ujian", isLoginSiswa, function (req, res, next) {
  new index(req, res).viewHasilUjian();
});
router.get("/contents/:id", function (req, res, next) {
  new index(req, res).contents();
});
router.get("/file/:id", isLoginSiswa, function (req, res, next) {
  new index(req, res).downloadBahanAjar();
});
router.get("/logout", function (req, res, next) {
  new index(req, res).logout();
});

module.exports = router;
