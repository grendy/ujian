var express = require("express");
var router = express.Router();
const admin = require("../controller/Admin");
const { isLoginAdmin } = require("../middleware/isLogin");

router.get("/", function (req, res, next) {
  res.redirect("/admin/login");
});
router.get("/login", function (req, res, next) {
  new admin(req, res).viewLogin();
});
router.post("/login", function (req, res, next) {
  new admin(req, res).login();
});
router.get("/home", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewHome();
});
router.get("/sekolah", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewSekolah();
});
router.put("/sekolah", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upSekolah();
});

router.get("/matapelajaran", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewMapel();
});
router.post("/matapelajaran", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addMapel();
});
router.delete("/matapelajaran/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delMapel();
});
router.put("/matapelajaran/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upMapel();
});
router.get("/form-matapelajaran/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormMapel();
});
router.post("/import-matapelajaran", isLoginAdmin, function (req, res, next) {
  new admin(req, res).importMapel();
});
router.get("/export-matapelajaran", isLoginAdmin, function (req, res, next) {
  new admin(req, res).exportMapel();
});
router.get("/format-matapelajaran", isLoginAdmin, function (req, res, next) {
  const file = `./public/files/format/mata-pelajaran.xlsx`;
  return res.download(file);
});

router.get("/kelas", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewKelas();
});
router.post("/import-kelas", isLoginAdmin, function (req, res, next) {
  new admin(req, res).importKelas();
});
router.get("/form-kelas/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormKelas();
});
router.post("/kelas", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addKelas();
});
router.delete("/kelas/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delKelas();
});
router.put("/kelas/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upKelas();
});
router.get("/export-kelas", isLoginAdmin, function (req, res, next) {
  new admin(req, res).exportKelas();
});
router.get("/format-kelas", isLoginAdmin, function (req, res, next) {
  const file = `./public/files/format/kelas.xlsx`;
  return res.download(file);
});

router.get("/ruangan", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewRuangan();
});
router.post("/ruangan", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addRuangan();
});
router.delete("/ruangan/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delRuangan();
});
router.put("/ruangan/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upRuangan();
});
router.get("/form-ruangan/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormRuangan();
});
router.post("/import-ruangan", isLoginAdmin, function (req, res, next) {
  new admin(req, res).importRuangan();
});
router.get("/export-ruangan", isLoginAdmin, function (req, res, next) {
  new admin(req, res).exportRuangan();
});
router.get("/format-ruangan", isLoginAdmin, function (req, res, next) {
  const file = `./public/files/format/ruangan.xlsx`;
  return res.download(file);
});

router.get("/peserta", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewPeserta();
});
router.get("/form-peserta/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormPeserta();
});
router.post("/peserta", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addPeserta();
});
router.put("/peserta/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upPeserta();
});
router.post("/import-peserta", isLoginAdmin, function (req, res, next) {
  new admin(req, res).importPeserta();
});
router.get("/export-peserta", isLoginAdmin, function (req, res, next) {
  new admin(req, res).exportPeserta();
});
router.delete("/peserta/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delPeserta();
});
router.get("/format-peserta", isLoginAdmin, function (req, res, next) {
  const file = `./public/files/format/siswa.xlsx`;
  return res.download(file);
});

router.get("/bahan-ajar", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewBahanAjar();
});
router.get("/form-bahan-ajar/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormBahanAjar();
});
router.post("/bahan-ajar", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addBahanAjar();
});
router.put("/bahan-ajar/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upBahanAjar();
});
router.delete("/bahan-ajar/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delBahanAjar();
});

router.get("/bank-soal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewBankSoal();
});
router.post("/bank-soal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addBankSoal();
});
router.delete("/bank-soal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delBankSoal();
});
router.put("/bank-soal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upBankSoal();
});
router.delete("/bank-soal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delBankSoal();
});
router.get("/form-bank-soal/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormBankSoal();
});

router.get("/data-soal/:id_banksoal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewDataSoal();
});
router.post(
  "/import-data-soal/:id_banksoal",
  isLoginAdmin,
  function (req, res, next) {
    new admin(req, res).importDataSoal();
  }
);
router.post("/data-soal/:id_banksoal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addDataSoal();
});
router.put(
  "/data-soal/:id_banksoal/:id",
  isLoginAdmin,
  function (req, res, next) {
    new admin(req, res).upDataSoal();
  }
);
router.delete("/data-soal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delDataSoal();
});
router.put(
  "/data-soal/:id_banksoal/:id",
  isLoginAdmin,
  function (req, res, next) {
    new admin(req, res).upDataSoal();
  }
);
router.get(
  "/form-data-soal/:idBankSoal/:id?",
  isLoginAdmin,
  function (req, res, next) {
    new admin(req, res).viewFormDataSoal();
  }
);
router.get("/format-soal", isLoginAdmin, function (req, res, next) {
  const file = `./public/files/format/soal.xlsx`;
  return res.download(file);
});

router.get("/file-pendukung/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFilePendukung();
});
router.post("/file-pendukung", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addFilePendukung();
});
router.delete("/file-pendukung/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delFilePendukung();
});

router.get("/jadwal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewJadwal();
});
router.post("/jadwal", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addJadwal();
});
router.delete("/jadwal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delJadwal();
});
router.put("/jadwal/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upJadwal();
});
router.get("/form-jadwal/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormJadwalUjian();
});

router.get("/reset", isLoginAdmin, function (req, res, next) {
  res.render("admin/reset", {
    title: "Reset",
  });
});

router.get("/kartu-ujian", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewKartuUjian();
});
router.post("/kartu-ujian", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addKartuUjian();
});
router.delete("/kartu-ujian/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delKartuUjian();
});
router.get("/cetak-kartu/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewCetakKartu();
});

router.get("/berita-acara/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewBeritaAcara();
});
router.post("/upload-berita-acara/:id", isLoginAdmin, function (req, res) {
  new admin(req, res).uploadBeritaAcara();
});
router.delete("/berita-acara/:id", isLoginAdmin, function (req, res) {
  new admin(req, res).delUploadBeritaAcara();
});

router.get("/daftar-hadir/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewDaftarHadir();
});
router.post("/daftar-hadir/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addDaftarHadir();
});
router.get("/cetak-daftar-hadir/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewCetakDaftarHadir();
});

router.get("/nilai-ujian", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewNilaiUjian();
});

router.get("/semua-nilai", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewSemuaNilai();
});
router.get("/detail-nilai", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewDetailNilai();
});

router.get("/pengumuman", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewPengumuman();
});
router.get("/form-pengumuman/:id?", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewFormPengumuman();
});
router.post("/pengumuman", isLoginAdmin, function (req, res, next) {
  new admin(req, res).addPengumuman();
});
router.put("/pengumuman/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).upPengumuman();
});
router.delete("/pengumuman/:id", isLoginAdmin, function (req, res, next) {
  new admin(req, res).delPengumuman();
});

router.get("/edit-profil", isLoginAdmin, function (req, res, next) {
  res.render("admin/edit-profil", {
    title: "Edit Profil",
  });
});
router.get("/pengaturan", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewPengaturan();
});

router.get("/logout", function (req, res, next) {
  new admin(req, res).logout();
});

router.get("/database", isLoginAdmin, function (req, res, next) {
  new admin(req, res).viewDatabase();
});
router.get("/backup", isLoginAdmin, function (req, res, next) {
  new admin(req, res).backup();
});
router.post("/restore", isLoginAdmin, function (req, res, next) {
  new admin(req, res).restore();
});
module.exports = router;
