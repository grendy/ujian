"use strict";

const createError = require("http-errors");
const path = require("path");
const express = require("express");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const logger = require("morgan");

const indexRouter = require("../routes/index");
const adminRouter = require("../routes/admin");
const superadminRouter = require("../routes/superadmin");
const fileUpload = require("express-fileupload");
const methodOverride = require("method-override");

module.exports = function (app) {
  // view engine setup
  app.set("views", path.join(__dirname, "/../views"));
  app.set("view engine", "pug");
  app.use(
    session({
      secret: "M1r34cl3",
      resave: true,
      saveUninitialized: true,
      cookie: { maxAge: 3600000 * 3 },
    })
  );
  // app.use(logger("dev"));
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, "../public/")));

  app.use(
    fileUpload({
      limits: { fileSize: process.env.MAX_UPLOAD_SIZE_MB * 1024 * 1024 },
      // tempFileDir: "./assets/",
      // useTempFiles: true,
    })
  );

  app.use(methodOverride("_method"));

  app.use("/", indexRouter);
  app.use("/admin", adminRouter);
  app.use("/superadmin", superadminRouter);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404));

    // error handler
    app.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render("error");
    });
  });
};
