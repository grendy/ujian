const express = require("express");
const app = express();

require("./startup/routes")(app);
require("./core/Jobs");

module.exports = app;
