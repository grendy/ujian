const jwt = require("jsonwebtoken");
// const winston    = require('../config/winston');

const isLoginSiswa = (req, res, next) => {
  try {
    const decoded = jwt.verify(req.cookies.token, "naikgaji");
    req.user = decoded;
    if (decoded.role == "siswa") {
      next();
    } else {
      throw new Error("data login error");
    }
  } catch (err) {
    if (err.message == "jwt expired") {
      req.session.message = "sesi telah habis! silahkan login kembali";
    } else if (err.message == "jwt must be provided") {
      req.session.message = "kamu belum login! silahkan login terlebih dahulu";
    } else {
      req.session.message = err.message;
    }
    res.cookie("token", "", { maxAge: 0, httpOnly: false });
    return res.redirect("/login");
  }
};
const isLoginAdmin = (req, res, next) => {
  try {
    const decoded = jwt.verify(req.cookies.token, "naikgaji");
    req.user = decoded;
    if (decoded.role == "admin") {
      next();
    } else {
      throw new Error("data login error");
    }
  } catch (err) {
    if (err.message == "jwt expired") {
      req.session.message = "sesi telah habis! silahkan login kembali";
    } else if (err.message == "jwt must be provided") {
      req.session.message = "kamu belum login! silahkan login terlebih dahulu";
    } else {
      req.session.message = err.message;
    }
    res.cookie("token", "", { maxAge: 0, httpOnly: false });
    return res.redirect("/admin/login");
  }
};
const isLoginSuperAdmin = (req, res, next) => {
  try {
    const decoded = jwt.verify(req.cookies.token, "naikgaji");
    req.user = decoded;
    if (decoded.role == "superadmin") {
      next();
    } else {
      throw new Error("data login error");
    }
  } catch (err) {
    if (err.message == "jwt expired") {
      req.session.message = "sesi telah habis! silahkan login kembali";
    } else if (err.message == "jwt must be provided") {
      req.session.message = "kamu belum login! silahkan login terlebih dahulu";
    } else {
      req.session.message = err.message;
    }
    res.cookie("token", "", { maxAge: 0, httpOnly: false });
    return res.redirect("/superadmin/login");
  }
};

module.exports.isLoginSiswa = isLoginSiswa;
module.exports.isLoginAdmin = isLoginAdmin;
module.exports.isLoginSuperAdmin = isLoginSuperAdmin;
