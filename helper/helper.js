const fs = require("fs");
const deleteFile = (path) => {
  try {
    fs.unlinkSync(path);
    //file removed
  } catch (err) {
    console.error("delete err = " + err);
    throw new Error(err.message);
  }
};
module.exports.deleteFile = deleteFile;
