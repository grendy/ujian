// 'use strict';
const Controller = require("../core/Controller"),
  stream = require("stream"),
  bcrypt = require("bcrypt"),
  mongoose = require("mongoose"),
  fs = require("fs"),
  path = require("path"),
  jwt = require("jsonwebtoken"),
  xlsx = require("xlsx"),
  {
    Schools,
    Dinas,
    Announcement,
    ExamCards,
    Schedulers,
    NewsEvents,
    Students,
    BankSoals,
    Soals,
    SupportFiles,
    Scores,
    Materials,
    NewsEventFiles,
  } = require("../models/Schemas"),
  config = require("config");
var _ = require("lodash");

class SuperAdmin extends Controller {
  constructor(req, res) {
    super(res);
    this.req = req;
  }

  async viewLogin() {
    try {
      if (this.req.cookies.token) {
        return this.res.redirect("/superadmin/home");
      }
      let data = {
        title: "Login",
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/login", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async login() {
    try {
      let dinas = await Dinas.findOne(this.req.body);
      if (dinas) {
        const token = jwt.sign(
          {
            role: "superadmin",
            kode: dinas.kode,
            nip: dinas.nip,
            nama: dinas.nama,
            jabatan: dinas.jabatan,
            tlp: dinas.tlp,
            email: dinas.email,
          },
          "naikgaji",
          {
            expiresIn: 18000,
          }
        );
        this.res.cookie("token", token, {
          maxAge: 1000 * 60 * 60 * 5,
          httpOnly: false,
        });
        this.res.redirect("/superadmin/home");
      } else {
        this.req.session.message = {
          message: "Username atau password salah!",
          type: "danger",
        };
        this.res.redirect("/superadmin/login");
      }
    } catch (err) {
      console.error("err login: ", err);
    }
  }

  async viewHome() {
    try {
      let data = {
        title: "Super Administrator",
        user: this.req.user,
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/index", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewSekolah() {
    try {
      let data = {
        title: "Data Sekolah",
        user: this.req.user,
        data: await Schools.find({}).sort({
          _id: -1,
        }),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/sekolah", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewFormSekolah() {
    try {
      let data = {
        title: "Tambah Sekolah",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Sekolah";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const sekolah = await Schools.findOne({
          _id: this.req.params.id,
        });
        if (sekolah == null) return this.res.redirect("/superadmin/sekolah");
        data.data = sekolah;
      }
      this.res.render("superadmin/sekolah-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async addSekolah() {
    try {
      await Schools.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah sekolah",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/sekolah");
    }
  }
  async upSekolah() {
    try {
      await Schools.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah sekolah",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/sekolah");
    }
  }
  async delSekolah() {
    try {
      const peserta = await Schools.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus peserta!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/sekolah");
    }
  }
  async importSekolah() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      await Schools.create(temp);
      this.req.session.message = {
        message: "berhasil mengimport sekolah",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport sekolah!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/sekolah");
    }
  }
  async exportSekolah() {
    let data = await Schools.find({})
      .sort({
        _id: -1,
      })
      .select("-_id -__v")
      .lean();
    let ws = xlsx.utils.json_to_sheet(data);
    let wb = xlsx.utils.book_new();

    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    let file = xlsx.write(wb, {
      type: "buffer",
    });
    this.res.writeHead(200, {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": "attachment; filename=sekolah.xlsx",
    });
    this.res.end(file);
  }

  async viewInfoSekolah() {
    try {
      let data = {
        title: "Detail Sekolah",
        user: this.req.user,
        listrik_sumber: [
          "Tidak Ada",
          "PLN",
          "Diesel",
          "Tenaga Surya",
          "PLN Diesel",
          "Biogas",
          "Lainnya",
        ],
        isp: [
          "Tidak Ada",
          "Jardiknas",
          "Telkom Speedy",
          "Telkom Astinet",
          "Telkomsel Flash",
          "Indosat Mentari",
          "Indosat IM3",
          "Indosat IM2",
          "Indosat IM2 (Satelit)",
          "XL (GSM)",
          "XL (Serat Optik)",
          "Smartfren",
          "Esia AHA",
          "CBN",
          "Centrin (Kabel)",
          "Centrin (Wavelan)",
          "Biznet (Kabel)",
          "Biznet (Serat Optik)",
          "Firstmedia",
          "Linknet",
          "CNI (Wavelan)",
          "CNI (Serat Optik)",
          "3 (Tri)",
          "Axis",
          "Lainnya (Kabel)",
          "Lainnya (Serat Optik)",
          "Lainnya (Wavelan)",
          "Lainnya (Satelit)",
          "Lainnya",
        ],
        data: await Schools.findOne({
          _id: this.req.params.id,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/detail", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewPeserta() {
    try {
      let data = {
        title: "Data Peserta",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
      };

      if (!_.isEmpty(this.req.query)) {
        data.data = await Students.find({
          id_sekolah: this.req.query.sekolah,
          kelas: this.req.query.kelas,
        }).sort({
          nama: 1,
        });
      } else {
        data.data = await Students.find({}).sort({
          nama: 1,
        });
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/peserta", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async updatePeserta() {
    try {
      let data = {
        title: "Edit Peserta",
        user: this.req.user,
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/peserta-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewBahanAjar() {
    try {
      let data = {
        title: "Data Bahan Ajar",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
        // data: await Materials.find({}),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await Materials.find({
          kelas: this.req.query.kelas,
          id_sekolah: this.req.query.sekolah,
        });
      } else {
        data.data = await Materials.find({});
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/bahan-ajar", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async updateBahanAjar() {
    try {
      let data = {
        title: "Edit Bahan Ajar",
        user: this.req.user,
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/bahan-ajar-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewTimTeknis() {
    try {
      let data = {
        title: "Data Tim Teknis",
        user: this.req.user,
        data: await Dinas.find({}).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/tim-teknis", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addTimTeknis() {
    try {
      await Dinas.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah tim teknis",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah tim teknis!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/tim-teknis");
    }
  }
  async importTimTeknis() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      await Dinas.create(temp);
      this.req.session.message = {
        message: "berhasil mengimport tim teknis",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport tim teknis!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/tim-teknis");
    }
  }
  async upTimTeknis() {
    try {
      await Dinas.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah tim teknis",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah tim teknis!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/tim-teknis");
    }
  }
  async delTimTeknis() {
    try {
      await Dinas.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus tim teknis",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus tim teknis!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/tim-teknis");
    }
  }
  async formTimTeknis() {
    try {
      let data = {
        title: "Tambah Tim Teknis",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Tim Teknis";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const sekolah = await Dinas.findOne({
          _id: this.req.params.id,
        });
        if (sekolah == null) return this.res.redirect("/superadmin/tim-teknis");
        data.data = sekolah;
      }
      this.res.render("superadmin/tim-teknis-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewBankSoal() {
    try {
      let data = {
        title: "Bank Soal",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
        data: await BankSoals.find({})
          .sort({
            _id: -1,
          })
          .limit(10),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await BankSoals.find({
          id_sekolah: this.req.query.sekolah,
        }).sort({
          _id: -1,
        });
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/bank-soal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewDataSoal() {
    try {
      let data = {
        title: "Data Soal",
        user: this.req.user,
        bankSoal: await BankSoals.findOne({
          _id: this.req.params.id_banksoal,
        }),
        data: await Soals.find({
          id_banksoal: this.req.params.id_banksoal,
        }).sort({
          _id: -1,
        }),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/data-soal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewFilePendukung() {
    try {
      let data = {
        title: "File Pendukung",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
        data: await SupportFiles.find()
          .sort({
            _id: -1,
          })
          .limit(10),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await SupportFiles.find({
          id_sekolah: this.req.query.sekolah,
        }).sort({
          _id: -1,
        });
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/file-pendukung", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewJadwal() {
    try {
      let data = {
        title: "Jadwal Ujian",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await Schedulers.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.query.sekolah),
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]);
      } else {
        data.data = await Schedulers.aggregate([
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]);
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/jadwal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewKartuUjian() {
    try {
      let data = {
        title: "Kartu Ujian",
        user: this.req.user,
        data: await Dinas.find({}).sort({
          _id: -1,
        }),
        sekolah: await Schools.find({}).sort("npsn"),
      };

      if (!_.isEmpty(this.req.query)) {
        data.data = await ExamCards.find({
          id_sekolah: this.req.query.sekolah,
        });
      } else {
        data.data = await ExamCards.find();
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/kartu-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async delKartuUjian() {
    try {
      await ExamCards.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus kartu ujian!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus kartu ujian!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/kartu-ujian");
    }
  }
  async viewCetakKartu() {
    try {
      let data = {
        title: "Cetak Kartu Ujian",
        user: this.req.user,
        data: await ExamCards.aggregate([
          {
            $match: {
              _id: new mongoose.Types.ObjectId(this.req.params.id),
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "kelas",
              foreignField: "kelas",
              as: "student",
            },
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/cetak-kartu", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewBeritaAcara() {
    try {
      let data = {
        title: "Berita Acara",
        user: this.req.user,
        data: await NewsEvents.aggregate([
          {
            $lookup: {
              from: "newseventfiles",
              localField: "_id",
              foreignField: "id_newsevent",
              as: "newseventfile",
            },
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/berita-acara", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addBeritaAcara() {
    try {
      const file = this.req.files.file;
      if (file.mimetype !== "application/pdf")
        throw new Error("ekstensi harus pdf");
      await NewsEvents.create({
        catatan: this.req.body.catatan,
        file: file.data.toString("base64"),
      });

      this.req.session.message = {
        message: "berhasil upload file berita acara",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal upload file berita acara!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/berita-acara");
    }
  }
  async delBeritaAcara() {
    try {
      await NewsEvents.deleteOne({
        _id: this.req.params.id,
      });
      await NewsEventFiles.deleteMany({
        id_newsevent: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus berita acara!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus berita acara!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/berita-acara");
    }
  }
  async downloadBeritaAcara() {
    const data = await NewsEventFiles.findOne({
      _id: this.req.params.id,
    });
    var fileContents = Buffer.from(data.file, "base64");
    var readStream = new stream.PassThrough();
    readStream.end(fileContents);
    this.res.set("Content-disposition", "attachment; filename=" + data.nama);
    return readStream.pipe(this.res);
  }

  async viewDaftarHadir() {
    try {
      if (this.req.params.id) {
        const data = await Schedulers.findOne({ _id: this.req.params.id });
        var fileContents = Buffer.from(data.file_daftar_hadir, "base64");
        var readStream = new stream.PassThrough();
        readStream.end(fileContents);
        this.res.set("Content-disposition", "attachment; filename=" + data._id);
        return readStream.pipe(this.res);
      }
      let data = {
        title: "Daftar Hadir",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await Schedulers.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.query.sekolah),
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]);
      } else {
        data.data = await Schedulers.aggregate([
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]);
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/daftar-hadir", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewCetakDaftarHadir() {
    try {
      let data = {
        title: "Cetak Daftar Hadir",
        user: this.req.user,
        data: await Schedulers.aggregate([
          {
            $match: {
              $and: [
                {
                  _id: new mongoose.Types.ObjectId(this.req.params.id),
                },
              ],
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "banksoal.kelas",
              foreignField: "kelas",
              as: "student",
            },
          },
          {
            $sort: {
              "student.name": -1,
            },
          },
        ]),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/cetak-daftar-hadir", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewNilai() {
    try {
      let data = {
        title: "Nilai Ujian",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
      };
      if (!_.isEmpty(this.req.query)) {
        data.data = await Scores.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "nisn",
              foreignField: "nisn",
              as: "student",
            },
          },
          {
            $match: {
              "student.kelas": this.req.query.kelas,
            },
          },
        ]);
      } else {
        data.data = await Scores.aggregate([
          {
            $lookup: {
              from: "students",
              localField: "nisn",
              foreignField: "nisn",
              as: "student",
            },
          },
        ]);
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/nilai-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewSemuaNilai() {
    try {
      let data = {
        title: "Nilai Ujian",
        user: this.req.user,
        sekolah: await Schools.find({}).sort("npsn"),
        data: await Dinas.find({}).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/semua-nilai", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewDetailNilai() {
    try {
      if (!this.req.query.kelas || !this.req.query.kode)
        return this.res.redirect("/superadmin/semua-nilai");

      let data = {
        title: "Semua Nilai",
        rekap: `Rekap Nilai ${this.req.query.kode} - Kelas ${this.req.query.kelas}`,
        user: this.req.user,
        mapel: await Scores.find({
          id_sekolah: this.req.query.sekolah,
        }).distinct("mapel"),
      };
      let nilai = await Students.aggregate([
        {
          $match: {
            $and: [
              {
                id_sekolah: new mongoose.Types.ObjectId(this.req.query.sekolah),
              },
              {
                kelas: this.req.query.kelas,
              },
            ],
          },
        },
        {
          $lookup: {
            from: "scores",
            localField: "nisn",
            foreignField: "nisn",
            as: "scores",
          },
        },
        {
          $match: {
            "scores.kode_ujian": this.req.query.kode,
          },
        },
        {
          $sort: {
            "scores.mapel": 1,
          },
        },
      ]);
      for (let i = 0; i < nilai.length; i++) {
        nilai[i].jml = _.sumBy(nilai[i].scores, "nilai");
        nilai[i].mean = _.meanBy(nilai[i].scores, "nilai");
        if (nilai[i].mean >= 81 && nilai[i].mean <= 100) {
          nilai[i].sikap = "A";
        } else if (nilai[i].mean >= 61 && nilai[i].mean <= 80) {
          nilai[i].sikap = "B";
        } else if (nilai[i].mean >= 41 && nilai[i].mean <= 60) {
          nilai[i].sikap = "C";
        } else if (nilai[i].mean >= 21 && nilai[i].mean <= 40) {
          nilai[i].sikap = "D";
        } else if (nilai[i].mean >= 0 && nilai[i].mean <= 20) {
          nilai[i].sikap = "E";
        }
      }
      data.data = nilai;
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/detail-nilai", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewPengumuman() {
    try {
      let data = {
        title: "Daftar Pengumuman",
        user: this.req.user,
        data: await Announcement.find({
          utk: "admin",
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("superadmin/pengumuman", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewFormPengumuman() {
    try {
      let data = {
        title: "Tambah Pengumuman",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Pengumuman";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const sekolah = await Announcement.findOne({
          _id: this.req.params.id,
        });
        if (sekolah == null) return this.res.redirect("/superadmin/pengumuman");
        data.data = sekolah;
      }
      this.res.render("superadmin/pengumuman-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addPengumuman() {
    try {
      await Announcement.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah pengumuman",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/pengumuman");
    }
  }
  async upPengumuman() {
    try {
      await Announcement.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah pengumuman",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/pengumuman");
    }
  }
  async delPengumuman() {
    try {
      await Announcement.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus pengumuman!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/superadmin/pengumuman");
    }
  }

  async logout() {
    try {
      this.res.cookie("token", "", {
        maxAge: 0,
        httpOnly: false,
      });
      this.res.redirect("/superadmin/login");
    } catch (err) {
      console.error("err: ", err);
    }
  }
}

module.exports = SuperAdmin;
