"use strict";
require("../models/Mongoose");
const Controller = require("../core/Controller"),
  stream = require("stream"),
  mongoose = require("mongoose"),
  bcrypt = require("bcrypt"),
  fs = require("fs"),
  path = require("path"),
  moment = require("moment"),
  jwt = require("jsonwebtoken"),
  xlsx = require("xlsx"),
  { deleteFile } = require("../helper/helper"),
  {
    Schools,
    Students,
    Classes,
    Rooms,
    Mapels,
    Materials,
    BankSoals,
    SupportFiles,
    Soals,
    Schedulers,
    Scores,
    Announcement,
    ExamCards,
    NewsEvents,
    NewsEventFiles,
    Dinas,
  } = require("../models/Schemas"),
  salt = bcrypt.genSaltSync(10),
  config = require("config");
var _ = require("lodash");

class Admin extends Controller {
  constructor(req, res) {
    super(res);
    this.req = req;
  }

  async viewLogin() {
    try {
      if (this.req.cookies.token) {
        return this.res.redirect("/admin/home");
      }
      let data = {
        title: "Login",
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/login", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async login() {
    try {
      let sekolah = await Schools.findOne(this.req.body);
      if (sekolah) {
        const token = jwt.sign(
          {
            _id: sekolah._id,
            role: "admin",
            npsn: sekolah.npsn,
            nama_sekolah: sekolah.nama_sekolah,
            kode_an: sekolah.kode_an,
            email: sekolah.email,
          },
          "naikgaji",
          {
            expiresIn: 18000,
          }
        );
        this.res.cookie("token", token, {
          maxAge: 1000 * 60 * 60 * 5,
          httpOnly: false,
        });
        this.res.redirect("/admin/home");
      } else {
        this.req.session.message = {
          message: "Username atau password salah!",
          type: "danger",
        };
        this.res.redirect("/admin/login");
      }
    } catch (err) {
      console.error("err login: ", err);
    }
  }

  async viewHome() {
    try {
      let data = {
        title: "Administrator",
        user: this.req.user,
        data: await Announcement.find({
          utk: "admin",
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/index", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewSekolah() {
    try {
      let data = {
        title: "Data Sekolah",
        user: this.req.user,
        listrik_sumber: [
          "Tidak Ada",
          "PLN",
          "Diesel",
          "Tenaga Surya",
          "PLN Diesel",
          "Biogas",
          "Lainnya",
        ],
        isp: [
          "Tidak Ada",
          "Jardiknas",
          "Telkom Speedy",
          "Telkom Astinet",
          "Telkomsel Flash",
          "Indosat Mentari",
          "Indosat IM3",
          "Indosat IM2",
          "Indosat IM2 (Satelit)",
          "XL (GSM)",
          "XL (Serat Optik)",
          "Smartfren",
          "Esia AHA",
          "CBN",
          "Centrin (Kabel)",
          "Centrin (Wavelan)",
          "Biznet (Kabel)",
          "Biznet (Serat Optik)",
          "Firstmedia",
          "Linknet",
          "CNI (Wavelan)",
          "CNI (Serat Optik)",
          "3 (Tri)",
          "Axis",
          "Lainnya (Kabel)",
          "Lainnya (Serat Optik)",
          "Lainnya (Wavelan)",
          "Lainnya (Satelit)",
          "Lainnya",
        ],
        data: await Schools.findOne({
          _id: this.req.user._id,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/sekolah", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async upSekolah() {
    try {
      await Schools.updateOne(
        {
          _id: this.req.user._id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah identitas sekolah",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah identitas sekolah!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/sekolah");
    }
  }

  async viewMapel() {
    try {
      let data = {
        title: "Mata Pelajaran",
        user: this.req.user,
        data: await Mapels.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/matapelajaran", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addMapel() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await Mapels.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah mapel",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah mapel!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/matapelajaran");
    }
  }
  async delMapel() {
    try {
      const peserta = await Mapels.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus mapel!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus mapel!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/matapelajaran");
    }
  }
  async upMapel() {
    try {
      await Mapels.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah mapel",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah mapel!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/matapelajaran");
    }
  }
  async viewFormMapel() {
    try {
      let data = {
        title: "Tambah Mata Pelajaran",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Mata Pelajaran";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Mapels.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/matapelajaran");
        data.data = psrta;
      }
      this.res.render("admin/matapelajaran-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async importMapel() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      for (let i = 0; i < temp.length; i++) {
        temp[i].id_sekolah = this.req.user._id;
        await Mapels.create(temp[i]);
      }
      this.req.session.message = {
        message: "berhasil mengimport mapel",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport mapel!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/matapelajaran");
    }
  }
  async exportMapel() {
    let data = await Mapels.find({
      id_sekolah: this.req.user._id,
    })
      .sort({
        _id: -1,
      })
      .select("-id_sekolah -_id -__v")
      .lean();
    let ws = xlsx.utils.json_to_sheet(data);
    let wb = xlsx.utils.book_new();

    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    let file = xlsx.write(wb, {
      type: "buffer",
    });
    this.res.writeHead(200, {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": "attachment; filename=mata-pelajaran.xlsx",
    });
    this.res.end(file);
  }

  async viewKelas() {
    try {
      let data = {
        title: "Kelas",
        user: this.req.user,
        data: await Classes.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/kelas", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async importKelas() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
        cellStyles: true,
      });
      // console.log(file.Sheets["Sheet1"]);
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"], {
        cellHTML: true,
        cellStyles: true,
      });
      for (let i = 0; i < temp.length; i++) {
        temp[i].id_sekolah = this.req.user._id;
        await Classes.create(temp[i]);
      }
      this.req.session.message = {
        message: "berhasil mengimport kelas",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport kelas!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kelas");
    }
  }
  async viewFormKelas() {
    try {
      let data = {
        title: "Tambah Kelas",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Kelas";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Classes.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/kelas");
        data.data = psrta;
      }
      this.res.render("admin/kelas-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addKelas() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await Classes.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah kelas",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah kelas!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kelas");
    }
  }
  async delKelas() {
    try {
      const peserta = await Classes.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus kelas!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus kelas!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kelas");
    }
  }
  async upKelas() {
    try {
      await Classes.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah kelas",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah kelas!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kelas");
    }
  }
  async exportKelas() {
    let data = await Classes.find({
      id_sekolah: this.req.user._id,
    })
      .sort({
        _id: -1,
      })
      .select("-id_sekolah -_id -__v")
      .lean();
    let ws = xlsx.utils.json_to_sheet(data);
    let wb = xlsx.utils.book_new();

    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    let file = xlsx.write(wb, {
      type: "buffer",
    });
    this.res.writeHead(200, {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": "attachment; filename=kelas.xlsx",
    });
    this.res.end(file);
  }

  async viewRuangan() {
    try {
      let data = {
        title: "Ruangan",
        user: this.req.user,
        data: await Rooms.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/ruangan", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addRuangan() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await Rooms.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah ruangan",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah ruangan!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/ruangan");
    }
  }
  async delRuangan() {
    try {
      const peserta = await Rooms.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus ruangan!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus ruangan!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/ruangan");
    }
  }
  async upRuangan() {
    try {
      await Rooms.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah ruangan",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah ruangan!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/ruangan");
    }
  }
  async viewFormRuangan() {
    try {
      let data = {
        title: "Tambah Ruangan",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Ruangan";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Rooms.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/ruangan");
        data.data = psrta;
      }
      this.res.render("admin/ruangan-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async importRuangan() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      for (let i = 0; i < temp.length; i++) {
        temp[i].id_sekolah = this.req.user._id;
        await Rooms.create(temp[i]);
      }
      this.req.session.message = {
        message: "berhasil mengimport ruangan",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport ruangan!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/ruangan");
    }
  }
  async exportRuangan() {
    let data = await Rooms.find({
      id_sekolah: this.req.user._id,
    })
      .sort({
        _id: -1,
      })
      .select("-id_sekolah -_id -__v")
      .lean();
    let ws = xlsx.utils.json_to_sheet(data);
    let wb = xlsx.utils.book_new();

    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    let file = xlsx.write(wb, {
      type: "buffer",
    });
    this.res.writeHead(200, {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": "attachment; filename=ruangan.xlsx",
    });
    this.res.end(file);
  }

  async viewPeserta() {
    try {
      let data = {
        title: "Peserta",
        user: this.req.user,
        data: await Students.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/peserta", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewFormPeserta() {
    try {
      let data = {
        title: "Tambah Peserta",
        user: this.req.user,
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
        ruangan: await Rooms.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Peserta";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Students.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/peserta");
        data.data = psrta;
      }
      this.res.render("admin/form-peserta", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addPeserta() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await Students.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah peserta",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/peserta");
    }
  }
  async upPeserta() {
    try {
      await Students.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah peserta",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/peserta");
    }
  }
  async importPeserta() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      for (let i = 0; i < temp.length; i++) {
        temp[i].id_sekolah = this.req.user._id;
        await Students.create(temp[i]);
      }
      this.req.session.message = {
        message: "berhasil mengimport peserta",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/peserta");
    }
  }
  async exportPeserta() {
    let data = await Students.find({
      id_sekolah: this.req.user._id,
    })
      .sort({
        _id: -1,
      })
      .select("-_id -__v")
      .lean();
    let ws = xlsx.utils.json_to_sheet(data);
    let wb = xlsx.utils.book_new();

    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    let file = xlsx.write(wb, {
      type: "buffer",
    });
    this.res.writeHead(200, {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": "attachment; filename=siswa.xlsx",
    });
    this.res.end(file);
  }
  async delPeserta() {
    try {
      const peserta = await Students.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus peserta!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus peserta!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/peserta");
    }
  }

  async viewBahanAjar() {
    try {
      let data = {
        title: "Bahan Ajar",
        user: this.req.user,
        data: await Materials.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/bahan-ajar", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewFormBahanAjar() {
    try {
      let data = {
        title: "Tambah Bahan Ajar",
        user: this.req.user,
        action: "",
        mapel: await Mapels.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
      };
      if (this.req.params.id) {
        data.title = "Edit Bahan Ajar";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Materials.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/bahan-ajar");
        data.data = psrta;
      }
      this.res.render("admin/bahan-ajar-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addBahanAjar() {
    try {
      if (
        this.req.files.file.mimetype !==
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
        this.req.files.file.mimetype !==
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document" &&
        this.req.files.file.mimetype !== "application/pdf" &&
        this.req.files.file.mimetype !== "image/jpeg" &&
        this.req.files.file.mimetype !== "image/jpg" &&
        this.req.files.file.mimetype !== "image/png" &&
        this.req.files.file.mimetype !== "video/mp4"
      )
        throw new Error("ekstensi harus docx/pdf/jpg/png/mp4");

      this.req.session.message = {
        message: "berhasil menambah bahan ajar",
        type: "success",
      };
      this.req.body.file = this.req.files.file.data.toString("base64");
      this.req.body.id_sekolah = this.req.user._id;
      await Materials.create(this.req.body);
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah bahan ajar!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bahan-ajar");
    }
  }
  async upBahanAjar() {
    try {
      if (this.req.files) {
        if (
          this.req.files.file.mimetype !==
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
          this.req.files.file.mimetype !==
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document" &&
          this.req.files.file.mimetype !== "application/pdf" &&
          this.req.files.file.mimetype !== "image/jpeg" &&
          this.req.files.file.mimetype !== "image/jpg" &&
          this.req.files.file.mimetype !== "image/png" &&
          this.req.files.file.mimetype !== "video/mp4"
        )
          throw new Error("ekstensi harus docx/pdf/jpg/png/mp4");

        this.req.body.file = this.req.files.file.data.toString("base64");
      }
      await Materials.updateOne({ _id: this.req.params.id }, this.req.body);
      this.req.session.message = {
        message: "berhasil mengubah bahan ajar",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah bahan ajar!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bahan-ajar");
    }
  }
  async delBahanAjar() {
    try {
      await Materials.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus bahan ajar!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus bahan ajar!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bahan-ajar");
    }
  }

  async viewBankSoal() {
    try {
      let data = {
        title: "Bank Soal",
        user: this.req.user,
        data: await BankSoals.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/bank-soal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addBankSoal() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await BankSoals.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah bank soal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah bank soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bank-soal");
    }
  }
  async delBankSoal() {
    try {
      await BankSoals.deleteOne({
        _id: this.req.params.id,
      });
      await Soals.deleteMany({
        id_banksoal: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus bank soal!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus bank soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bank-soal");
    }
  }
  async upBankSoal() {
    try {
      await BankSoals.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah bank soal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah bank soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/bank-soal");
    }
  }
  async viewFormBankSoal() {
    try {
      let data = {
        title: "Tambah Bank Soal",
        user: this.req.user,
        action: "",
        mapel: await Mapels.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
      };
      if (this.req.params.id) {
        data.title = "Edit Bank Soal";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await BankSoals.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/bank-soal");
        data.data = psrta;
      }
      this.res.render("admin/bank-soal-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewDataSoal() {
    try {
      let data = {
        title: "Data Soal",
        user: this.req.user,
        bankSoal: await BankSoals.findOne({
          id_sekolah: this.req.user._id,
          _id: this.req.params.id_banksoal,
        }),
        data: await Soals.find({
          id_sekolah: this.req.user._id,
          id_banksoal: this.req.params.id_banksoal,
        }).sort({
          _id: -1,
        }),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/data-soal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async importDataSoal() {
    try {
      if (
        this.req.files.file.mimetype !==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      )
        throw new Error("ekstensi harus .xlsx");
      // baca buffer file
      const file = xlsx.read(this.req.files.file.data, {
        type: "buffer",
      });
      const temp = xlsx.utils.sheet_to_json(file.Sheets["Sheet1"]);
      for (let i = 0; i < temp.length; i++) {
        temp[i].id_sekolah = this.req.user._id;
        temp[i].id_banksoal = this.req.params.id_banksoal;
        await Soals.create(temp[i]);
      }
      this.req.session.message = {
        message: "berhasil mengimport data soal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengimport data soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/data-soal/" + this.req.params.id_banksoal);
    }
  }
  async addDataSoal() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      this.req.body.id_banksoal = this.req.params.id_banksoal;
      await Soals.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah soal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/data-soal/" + this.req.body.id_banksoal);
    }
  }
  async upDataSoal() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      this.req.body.id_banksoal = this.req.params.id_banksoal;
      await Soals.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mangubah soal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mangubah soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/data-soal/" + this.req.params.id_banksoal);
    }
  }
  async delDataSoal() {
    try {
      await Soals.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus data soal!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus data soal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("back");
    }
  }
  async viewFormDataSoal() {
    try {
      let data = {
        title: "Tambah Soal",
        user: this.req.user,
        action: "/" + this.req.params.idBankSoal,
        bankSoal: await BankSoals.findOne({
          id_sekolah: this.req.user._id,
          _id: this.req.params.idBankSoal,
        }).sort({
          _id: -1,
        }),
        filePendukung: await SupportFiles.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };
      if (this.req.params.id) {
        data.title = "Edit Soal";
        data.action = `/${this.req.params.idBankSoal}/${this.req.params.id}?_method=PUT`;
        const psrta = await Soals.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null)
          return this.res.redirect(
            "/admin/data-soal/" + this.req.user.idBankSoal
          );
        data.data = psrta;
      }
      this.res.render("admin/tambah-soal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewFilePendukung() {
    try {
      if (this.req.params.id) {
        const data = await SupportFiles.findOne({ nama: this.req.params.id });
        var fileContents = Buffer.from(data.file, "base64");
        var readStream = new stream.PassThrough();
        readStream.end(fileContents);
        this.res.set(
          "Content-disposition",
          "attachment; filename=" + data.nama
        );
        return readStream.pipe(this.res);
      }
      let data = {
        title: "File Pendukung",
        user: this.req.user,
        data: await SupportFiles.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/file-pendukung", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addFilePendukung() {
    try {
      const file = this.req.files.file;
      if (!file.length) {
        if (
          file.mimetype !== "image/jpeg" &&
          file.mimetype !== "image/jpg" &&
          file.mimetype !== "image/png"
        )
          throw new Error("ekstensi harus jpeg/jpg/png");
        await SupportFiles.create({
          id_sekolah: this.req.user._id,
          nama: file.name,
          file: file.data.toString("base64"),
        });
      } else {
        for (let i = 0; i < file.length; i++) {
          if (
            file[i].mimetype !== "image/jpeg" &&
            file[i].mimetype !== "image/jpg" &&
            file[i].mimetype !== "image/png"
          )
            throw new Error("ekstensi harus jpeg/jpg/png");
        }
        for (let i = 0; i < file.length; i++) {
          await SupportFiles.create({
            id_sekolah: this.req.user._id,
            nama: file[i].name,
            file: file[i].data.toString("base64"),
          });
        }
      }
      this.req.session.message = {
        message: "berhasil menambah file pendukung",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah file pendukung!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/file-pendukung");
    }
  }
  async delFilePendukung() {
    try {
      await SupportFiles.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus file pendukung!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus file pendukung!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("back");
    }
  }

  async viewJadwal() {
    try {
      let data = {
        title: "Jadwal Ujian",
        user: this.req.user,
        data: await Schedulers.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/jadwal", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addJadwal() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      this.req.body.tgl_mulai = new Date(this.req.body.tgl_mulai).getTime();
      this.req.body.tgl_selesai = new Date(this.req.body.tgl_selesai).getTime();
      await Schedulers.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah jadwal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah jadwal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/jadwal");
    }
  }
  async delJadwal() {
    try {
      await Schedulers.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus jadwal!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus jadwal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/jadwal");
    }
  }
  async upJadwal() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      this.req.body.tgl_mulai = new Date(this.req.body.tgl_mulai).getTime();
      this.req.body.tgl_selesai = new Date(this.req.body.tgl_selesai).getTime();
      this.req.body.status = 0;
      await Schedulers.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mangubah jadwal",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mangubah jadwal!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/jadwal");
    }
  }
  async viewFormJadwalUjian() {
    try {
      let data = {
        title: "Tambah Jadwal Ujian",
        user: this.req.user,
        action: "",
        banksoal: await BankSoals.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
        ruangan: await Rooms.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
      };
      if (this.req.params.id) {
        data.title = "Edit Jadwal Ujian";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const psrta = await Schedulers.findOne({
          _id: this.req.params.id,
        });
        if (psrta == null) return this.res.redirect("/admin/jadwal");
        data.data = psrta;
      }
      this.res.render("admin/jadwal-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async updateJadwalUjian() {
    try {
      let data = {
        title: "Edit Jadwal Ujian",
        user: this.req.user,
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/jadwal-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewNilaiUjian() {
    try {
      let data = {
        title: "Nilai Ujian",
        user: this.req.user,
        mapel: await Mapels.find({
          id_sekolah: this.req.user._id,
        }).sort({
          _id: -1,
        }),
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
      };
      if (_.isEmpty(this.req.query)) {
        data.data = await Scores.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "nisn",
              foreignField: "nisn",
              as: "student",
            },
          },
        ]);
      } else {
        data.data = await Scores.aggregate([
          {
            $match: {
              $and: [
                {
                  id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
                },
                {
                  mapel: this.req.query.mapel,
                },
              ],
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "nisn",
              foreignField: "nisn",
              as: "student",
            },
          },
          {
            $match: {
              "student.kelas": this.req.query.kelas,
            },
          },
        ]);
      }

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/nilai-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewKartuUjian() {
    try {
      let data = {
        title: "Kartu Ujian",
        user: this.req.user,
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
        data: await ExamCards.find({
          id_sekolah: this.req.user._id,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/kartu-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addKartuUjian() {
    try {
      this.req.body.id_sekolah = this.req.user._id;
      await ExamCards.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah kartu ujian",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah kartu ujian!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kartu-ujian");
    }
  }
  async delKartuUjian() {
    try {
      await ExamCards.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus kartu ujian!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus kartu ujian!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/kartu-ujian");
    }
  }
  async viewCetakKartu() {
    try {
      let data = {
        title: "Cetak Kartu Ujian",
        user: this.req.user,
        data: await ExamCards.aggregate([
          {
            $match: {
              _id: new mongoose.Types.ObjectId(this.req.params.id),
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "kelas",
              foreignField: "kelas",
              as: "student",
            },
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/cetak-kartu", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewBeritaAcara() {
    try {
      if (this.req.params.id) {
        const data = await NewsEvents.findOne({ _id: this.req.params.id });
        var fileContents = Buffer.from(data.file, "base64");
        var readStream = new stream.PassThrough();
        readStream.end(fileContents);
        this.res.set(
          "Content-disposition",
          "attachment; filename=" + data.nama
        );
        return readStream.pipe(this.res);
      }
      let data = {
        title: "Berita Acara",
        user: this.req.user,
        data: await NewsEvents.aggregate([
          {
            $lookup: {
              from: "newseventfiles",
              localField: "_id",
              foreignField: "id_newsevent",
              as: "newseventfile",
              pipeline: [
                {
                  $match: {
                    id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
                  },
                },
              ],
            },
            // id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/berita-acara", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async uploadBeritaAcara() {
    try {
      const file = this.req.files.file;
      if (file.mimetype !== "application/pdf")
        throw new Error("ekstensi harus pdf");
      await NewsEventFiles.create({
        id_newsevent: this.req.params.id,
        id_sekolah: this.req.user._id,
        nama: this.req.user.nama_sekolah,
        file: file.data.toString("base64"),
      });

      this.req.session.message = {
        message: "berhasil upload file berita acara",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal upload file berita acara!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/berita-acara");
    }
  }
  async delUploadBeritaAcara() {
    try {
      await NewsEventFiles.deleteOne({
        id_newsevent: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus upload berita acara!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus upload berita acara!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/berita-acara");
    }
  }

  async viewDaftarHadir() {
    try {
      if (this.req.params.id) {
        const data = await Schedulers.findOne({ _id: this.req.params.id });
        var fileContents = Buffer.from(data.file_daftar_hadir, "base64");
        var readStream = new stream.PassThrough();
        readStream.end(fileContents);
        this.res.set("Content-disposition", "attachment; filename=" + data._id);
        return readStream.pipe(this.res);
      }
      let data = {
        title: "Daftar Hadir",
        user: this.req.user,
        data: await Schedulers.aggregate([
          {
            $match: {
              id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/daftar-hadir", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addDaftarHadir() {
    try {
      const file = this.req.files.file;
      if (file.mimetype !== "application/pdf")
        throw new Error("ekstensi harus pdf");

      await Schedulers.updateOne(
        {
          _id: this.req.params.id,
        },
        {
          file_daftar_hadir: file.data.toString("base64"),
        }
      );

      this.req.session.message = {
        message: "berhasil menambah daftar hadir",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah daftar hadir!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/daftar-hadir");
    }
  }
  async viewCetakDaftarHadir() {
    try {
      let data = {
        title: "Cetak Daftar Hadir",
        user: this.req.user,
        data: await Schedulers.aggregate([
          {
            $match: {
              $and: [
                {
                  id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
                },
                {
                  _id: new mongoose.Types.ObjectId(this.req.params.id),
                },
              ],
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
          {
            $lookup: {
              from: "students",
              localField: "banksoal.kelas",
              foreignField: "kelas",
              as: "student",
            },
          },
          {
            $sort: {
              "student.name": -1,
            },
          },
        ]),
      };
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/cetak-daftar-hadir", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewSemuaNilai() {
    try {
      let data = {
        title: "Semua Nilai",
        user: this.req.user,
        kelas: await Classes.find({
          id_sekolah: this.req.user._id,
        }).distinct("kode"),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/semua-nilai", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewDetailNilai() {
    try {
      if (!this.req.query.kelas || !this.req.query.kode)
        return this.res.redirect("/admin/semua-nilai");

      let data = {
        title: "Semua Nilai",
        rekap: `Rekap Nilai ${this.req.query.kode} - Kelas ${this.req.query.kelas}`,
        user: this.req.user,
        mapel: await Scores.find({
          id_sekolah: this.req.user._id,
        }).distinct("mapel"),
      };
      let nilai = await Students.aggregate([
        {
          $match: {
            $and: [
              {
                id_sekolah: new mongoose.Types.ObjectId(this.req.user._id),
              },
              {
                kelas: this.req.query.kelas,
              },
            ],
          },
        },
        {
          $lookup: {
            from: "scores",
            localField: "nisn",
            foreignField: "nisn",
            as: "scores",
          },
        },
        {
          $match: {
            "scores.kode_ujian": this.req.query.kode,
          },
        },
        {
          $sort: {
            "scores.mapel": 1,
          },
        },
      ]);
      for (let i = 0; i < nilai.length; i++) {
        nilai[i].jml = _.sumBy(nilai[i].scores, "nilai");
        nilai[i].mean = _.meanBy(nilai[i].scores, "nilai");
        if (nilai[i].mean >= 81 && nilai[i].mean <= 100) {
          nilai[i].sikap = "A";
        } else if (nilai[i].mean >= 61 && nilai[i].mean <= 80) {
          nilai[i].sikap = "B";
        } else if (nilai[i].mean >= 41 && nilai[i].mean <= 60) {
          nilai[i].sikap = "C";
        } else if (nilai[i].mean >= 21 && nilai[i].mean <= 40) {
          nilai[i].sikap = "D";
        } else if (nilai[i].mean >= 0 && nilai[i].mean <= 20) {
          nilai[i].sikap = "E";
        }
      }
      data.data = nilai;
      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/detail-nilai", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewPengumuman() {
    try {
      let data = {
        title: "Daftar Pengumuman",
        user: this.req.user,
        data: await Announcement.find({
          utk: "siswa",
        }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/pengumuman", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewFormPengumuman() {
    try {
      let data = {
        title: "Tambah Pengumuman",
        user: this.req.user,
        action: "",
      };
      if (this.req.params.id) {
        data.title = "Edit Pengumuman";
        data.action = `/${this.req.params.id}?_method=PUT`;
        const sekolah = await Announcement.findOne({
          _id: this.req.params.id,
        });
        if (sekolah == null) return this.res.redirect("/admin/pengumuman");
        data.data = sekolah;
      }
      this.res.render("admin/pengumuman-module", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async addPengumuman() {
    try {
      await Announcement.create(this.req.body);
      this.req.session.message = {
        message: "berhasil menambah pengumuman",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menambah pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/pengumuman");
    }
  }
  async upPengumuman() {
    try {
      await Announcement.updateOne(
        {
          _id: this.req.params.id,
        },
        this.req.body
      );
      this.req.session.message = {
        message: "berhasil mengubah pengumuman",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal mengubah pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/pengumuman");
    }
  }
  async delPengumuman() {
    try {
      await Announcement.deleteOne({
        _id: this.req.params.id,
      });
      this.req.session.message = {
        message: "berhasil menghapus pengumuman!",
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal menghapus pengumuman!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/pengumuman");
    }
  }

  async viewPengaturan() {
    try {
      let data = {
        title: "Pengaturan",
        user: this.req.user,
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("admin/pengaturan", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewDatabase() {
    try {
      let data = {
        title: "Database",
      };
      this.res.render("admin/database", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async backup() {
    try {
      let data = {
        Dinas: await Dinas.find().lean(),
        Schools: await Schools.find().lean(),
        Students: await Students.find().lean(),
        Classes: await Classes.find().lean(),
        Rooms: await Rooms.find().lean(),
        Mapels: await Mapels.find().lean(),
        Materials: await Materials.find().lean(),
        BankSoals: await BankSoals.find().lean(),
        SupportFiles: await SupportFiles.find().lean(),
        Soals: await Soals.find().lean(),
        Schedulers: await Schedulers.find().lean(),
        Scores: await Scores.find().lean(),
        Announcement: await Announcement.find().lean(),
        ExamCards: await ExamCards.find().lean(),
        NewsEvents: await NewsEvents.find().lean(),
        NewsEventFiles: await NewsEventFiles.find().lean(),
      };

      var mimetype = "application/json";
      this.res.setHeader("Content-Type", mimetype);
      this.res.setHeader(
        "Content-disposition",
        "attachment; filename=backup.json"
      );
      this.res.json(data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async restore() {
    try {
      if (this.req.files.file.mimetype !== "application/json")
        throw new Error("ekstensi harus .json");
      let temp = JSON.parse(this.req.files.file.data.toString());
      // let countDinas = temp.dinas.length || 0;
      let gagal = {};

      gagal.dinas = await Dinas.insertMany(temp.Dinas, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.school = await Schools.insertMany(temp.Schools, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.students = await Students.insertMany(temp.Students, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.classes = await Classes.insertMany(temp.Classes, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.rooms = await Rooms.insertMany(temp.Rooms, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.mapels = await Mapels.insertMany(temp.Mapels, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.materials = await Materials.insertMany(temp.Materials, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.banksoals = await BankSoals.insertMany(temp.BankSoals, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.supportfiles = await SupportFiles.insertMany(temp.SupportFiles, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.soals = await Soals.insertMany(temp.Soals, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.schedulers = await Schedulers.insertMany(temp.Schedulers, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.scores = await Scores.insertMany(temp.Scores, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.announcement = await Announcement.insertMany(temp.Announcement, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.examcards = await ExamCards.insertMany(temp.ExamCards, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.newsevents = await NewsEvents.insertMany(temp.NewsEvents, {
        ordered: false,
        silent: true,
      })
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      gagal.newseventfiles = await NewsEventFiles.insertMany(
        temp.NewsEventFiles,
        {
          ordered: false,
          silent: true,
        }
      )
        .then(function () {
          return null;
        })
        .catch(function (err) {
          return err;
        });
      console.log(gagal);
      this.req.session.message = {
        message: gagal,
        type: "success",
      };
    } catch (err) {
      console.error("err: ", err);
      this.req.session.message = {
        message: "gagal restore data!, " + err.message,
        type: "danger",
      };
    } finally {
      this.req.session.save();
      this.res.redirect("/admin/database");
    }
  }

  async logout() {
    try {
      this.res.cookie("token", "", {
        maxAge: 0,
        httpOnly: false,
      });
      this.res.redirect("/admin/login");
    } catch (err) {
      console.error("err: ", err);
    }
  }
}

module.exports = Admin;
