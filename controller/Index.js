"use strict";
require("../models/Mongoose");
const Controller = require("../core/Controller"),
  stream = require("stream"),
  bcrypt = require("bcrypt"),
  fs = require("fs"),
  path = require("path"),
  moment = require("moment"),
  mongoose = require("mongoose"),
  jwt = require("jsonwebtoken"),
  {
    Students,
    Schedulers,
    Soals,
    BankSoals,
    Scores,
    Materials,
    Announcement,
    SupportFiles,
  } = require("../models/Schemas"),
  salt = bcrypt.genSaltSync(10),
  config = require("config");
var _ = require("lodash");

class Index extends Controller {
  constructor(req, res) {
    super(res);
    this.req = req;
  }

  async viewLogin() {
    try {
      if (this.req.cookies.token) {
        return this.res.redirect("/home");
      }
      let data = {
        title: "Login",
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("index", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async login() {
    try {
      let siswa = await Students.findOne(this.req.body);
      if (siswa) {
        const token = jwt.sign(
          {
            id_sekolah: siswa.id_sekolah,
            nisn: siswa.nisn,
            no_peserta: siswa.no_peserta,
            nama: siswa.nama,
            kelas: siswa.kelas,
            role: "siswa",
          },
          "naikgaji",
          {
            expiresIn: 18000,
          }
        );
        this.res.cookie("token", token, {
          maxAge: 1000 * 60 * 60 * 5,
          httpOnly: false,
        });
        this.res.redirect("/home");
      } else {
        this.req.session.message = "Username atau password salah!";
        this.res.redirect("/login");
      }
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewHome() {
    try {
      let data = {
        title: "Home",
        user: this.req.user,
        data: await Announcement.find({ utk: "siswa" }).sort({
          _id: -1,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/index", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async viewJadwalUjian() {
    try {
      let data = {
        title: "Jadwal Ujian",
        user: this.req.user,
      };
      const getSchedule = await Schedulers.aggregate([
        {
          $match: {
            id_sekolah: new mongoose.Types.ObjectId(this.req.user.id_sekolah),
            tgl_selesai: {
              $gt: new Date().getTime(),
            },
          },
        },
        {
          $lookup: {
            from: "banksoals",
            localField: "kode_banksoal",
            foreignField: "kode",
            as: "banksoal",
          },
        },
        {
          $match: {
            "banksoal.kelas": this.req.user.kelas,
          },
        },
      ]);
      const sdhUjian = await Scores.find({
        id_sekolah: this.req.user.id_sekolah,
        nisn: this.req.user.nisn,
      });
      for (let i = 0; i < getSchedule.length; i++) {
        const index = _.findIndex(sdhUjian, {
          id_schedule: getSchedule[i]._id,
        });
        if (index == -1) {
          getSchedule[i]["isDone"] = 0;
        } else {
          getSchedule[i]["isDone"] = 1;
        }
      }
      data.data = getSchedule;

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/jadwal-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewKonfirmUjian() {
    try {
      let data = {
        title: "Konfirmasi Ujian",
        user: this.req.user,
        data: await Schedulers.aggregate([
          {
            $match: {
              $and: [
                {
                  id_sekolah: new mongoose.Types.ObjectId(
                    this.req.user.id_sekolah
                  ),
                },
                { _id: new mongoose.Types.ObjectId(this.req.params.id) },
              ],
            },
          },
          {
            $lookup: {
              from: "banksoals",
              localField: "kode_banksoal",
              foreignField: "kode",
              as: "banksoal",
            },
          },
        ]),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/mulai-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewUjian() {
    try {
      let data = {
        title: "Ujian Berlangsung",
        user: this.req.user,
      };

      const schedule = await Schedulers.findOne({
        id_sekolah: this.req.user.id_sekolah,
        _id: this.req.params.id,
      });
      const banksoal = await BankSoals.findOne({
        id_sekolah: this.req.user.id_sekolah,
        kode: schedule.kode_banksoal,
      });
      const soal = await Soals.find({
        id_sekolah: this.req.user.id_sekolah,
        id_banksoal: banksoal._id,
      });
      const sf = this.shuffle(soal);
      data.data = _.take(sf, banksoal.jml_tampil);
      data.schedule = schedule;
      data.banksoal = banksoal;

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/mulai", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async selesaiUjian() {
    // console.log(this.req.body);
    const schedule = await Schedulers.aggregate([
      {
        $match: {
          $and: [
            {
              id_sekolah: new mongoose.Types.ObjectId(this.req.user.id_sekolah),
            },
            { _id: new mongoose.Types.ObjectId(this.req.params.id) },
          ],
        },
      },
      {
        $lookup: {
          from: "banksoals",
          localField: "kode_banksoal",
          foreignField: "kode",
          as: "banksoal",
        },
      },
    ]);
    let tmp = [];
    for (let i = 0; i < this.req.body.soal.length; i++) {
      tmp.push(mongoose.Types.ObjectId(this.req.body.soal[i].soal));
    }
    const getSoal = await Soals.find({
      _id: { $in: tmp },
    });
    // console.log(getSoal);
    let benar = 0;
    for (let i = 0; i < this.req.body.soal.length; i++) {
      if (
        _.find(getSoal, {
          _id: mongoose.Types.ObjectId(this.req.body.soal[i].soal),
          jawaban: this.req.body.soal[i].jawaban,
        })
      ) {
        benar++;
      }
    }
    const skor = (benar * 100) / schedule[0].banksoal[0].jml_tampil;
    let data = {
      id_sekolah: this.req.user.id_sekolah,
      id_schedule: this.req.params.id,
      nisn: this.req.user.nisn,
      kode_ujian: schedule[0].kode,
      mapel: schedule[0].banksoal[0].mapel,
      nilai: skor,
    };
    await Scores.create(data);
    this.res.redirect("/jadwal-ujian");
  }
  async viewBahanAjar() {
    try {
      let data = {
        title: "Bahan Ajar",
        user: this.req.user,
        data: await Materials.find({
          id_sekolah: this.req.user.id_sekolah,
          kelas: this.req.user.kelas,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/bahan-ajar", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewMateri() {
    try {
      let data = {
        title: "Materi",
        user: this.req.user,
        data: await Materials.findOne({ _id: this.req.params.id }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/lihat-materi", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }
  async viewHasilUjian() {
    try {
      let data = {
        title: "Hasil Ujian",
        user: this.req.user,
        data: await Scores.find({
          id_sekolah: this.req.user.id_sekolah,
          nisn: this.req.user.nisn,
        }),
      };

      if (_.has(this.req.session, "message")) {
        data.message = this.req.session.message;
        this.req.session.message = null;
        this.req.session.save();
      }
      this.res.render("siswa/hasil-ujian", data);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async contents() {
    try {
      const data = await SupportFiles.findOne({ nama: this.req.params.id });
      var fileContents = Buffer.from(data.file, "base64");
      var readStream = new stream.PassThrough();
      readStream.end(fileContents);
      this.res.set("Content-disposition", "attachment; filename=" + data.nama);
      return readStream.pipe(this.res);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async downloadBahanAjar() {
    try {
      const data = await Materials.findById(this.req.params.id);
      var fileContents = Buffer.from(data.file, "base64");

      var readStream = new stream.PassThrough();
      readStream.end(fileContents);

      this.res.set("Content-disposition", "attachment; filename=" + data.judul);

      readStream.pipe(this.res);
    } catch (err) {
      console.error("err: ", err);
    }
  }

  async logout() {
    try {
      this.res.cookie("token", "", { maxAge: 0, httpOnly: false });
      this.res.redirect("/login");
    } catch (err) {
      console.error("err: ", err);
    }
  }

  shuffle(array) {
    let currentIndex = array.length,
      randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex],
        array[currentIndex],
      ];
    }

    return array;
  }
}

module.exports = Index;
