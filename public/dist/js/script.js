$(function () {
    var ctx = $("#chart-sek2");
    var ctx2 = $("#chart-sek");
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [10, 5],
                backgroundColor: [
                    '#0abb87',
                    '#5d78ff'

                ],
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Sedang Ujian',
                'Ujian Aktif'
            ]
        },

    });

    var myBarChart = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: ['Siswa', 'Kelas', 'Soal', 'Nilai'],
            datasets: [{
                label: '',
                barPercentage: 0.5,
                minBarLength: 2,
                data: [100, 9, 150, 100],
                backgroundColor: [
                    '#f33d3d', '#f145ac', '#6c6afb', '#575f96'

                ],

            }],
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });


})