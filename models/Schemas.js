const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dinasSchema = new mongoose.Schema({
  kode: { type: String, required: true },
  provinsi: { type: String, required: true },
  kota: { type: String, required: true },
  jabatanTimTeknis: { type: String, required: true },
  nama: { type: String, required: true },
  nip: { type: Number, required: true },
  jabatan: { type: String, required: true },
  instansi: { type: String, required: true },
  tlp: { type: Number, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
});
const Dinas = mongoose.model("dinas", dinasSchema);

const schoolsSchema = new mongoose.Schema({
  npsn: { type: String, required: true, unique: true },
  nama_sekolah: { type: String, required: true },
  kode_an: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, default: "" },
  kota: { type: String, required: true },
  provinsi: { type: String, required: true },
  jenjang: { type: String, required: true },
  status: { type: String, required: true },
  status_pelaksanaan: { type: String, required: true },
  mode_pelaksanaan: { type: String, required: true },

  nama: { type: String, default: "" },
  nip: { type: String, default: "" },
  tlp: { type: String, default: "" },
  nama_praktor: { type: String, default: "" },
  email_praktor: { type: String, default: "" },
  tlp_praktor: { type: String, default: "" },
  nama_teknis: { type: String, default: "" },
  email_teknis: { type: String, default: "" },
  tlp_teknis: { type: String, default: "" },

  jumlah_server: { type: String, default: "" },
  jumlah_lab: { type: String, default: "" },
  jumlah_client_milik: { type: String, default: "" },
  jumlah_client_nonmilik: { type: String, default: "" },
  jumlah_switch: { type: String, default: "" },
  jumlah_access_point: { type: String, default: "" },
  listrik_sumber: { type: String, default: "" },
  listrik_daya: { type: String, default: "" },
  isp: { type: String, default: "" },
  internet_bandwidth_upload: { type: String, default: "" },
  internet_bandwidth_download: { type: String, default: "" },
});
const Schools = mongoose.model("schools", schoolsSchema);

const studentsSchema = new mongoose.Schema({
  nisn: { type: String, required: true },
  no_peserta: { type: String, required: true },
  nama: { type: String, required: true },
  kelas: { type: String, required: true },
  jenis_kelamin: { type: String, required: true },
  nama_orang_tua: { type: String, required: true },
  tempat_lahir: { type: String, required: true },
  tanggal_lahir: { type: String, required: true },
  password: { type: String, required: true },
  status: { type: String, required: true },
  jenjang: { type: String, required: true },
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  ruangan: { type: String, required: true },
});
const Students = mongoose.model("students", studentsSchema);

const classesSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kode: { type: String, required: true },
  nama: { type: String, required: true },
});
const Classes = mongoose.model("classes", classesSchema);

const roomsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kode: { type: String, required: true },
  nama: { type: String, required: true },
});
const Rooms = mongoose.model("rooms", roomsSchema);

const mapelsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kode: { type: String, required: true },
  nama: { type: String, required: true },
});
const Mapels = mongoose.model("mapels", mapelsSchema);

const materialsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  mapel: { type: String, required: true },
  judul: { type: String, required: true },
  isi: { type: String, required: true },
  kelas: { type: String, required: true },
  terbit: { type: String, required: true },
  link_yt: { type: String },
  file: { type: String, default: null },
});
const Materials = mongoose.model("materials", materialsSchema);

const BankSoalsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kode: { type: String, required: true },
  mapel: { type: String, required: true },
  kelas: { type: String, required: true },
  nama_guru: { type: String, required: true },
  jml_soal: { type: String, required: true },
  jml_tampil: { type: String, required: true },
  status: { type: String, required: true },
});
const BankSoals = mongoose.model("banksoals", BankSoalsSchema);

const SupportFilesSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  nama: { type: String, required: true, unique: true },
  file: { type: String, required: true },
});
const SupportFiles = mongoose.model("supportfiles", SupportFilesSchema);

const SoalsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  id_banksoal: { type: Schema.Types.ObjectId, required: true },
  isi_soal: { type: String, required: true },
  file: { type: String, default: null },
  jawaban: { type: String, required: true },
  pilihanA: { type: String, default: null },
  imgPilihanA: { type: String, default: null },
  pilihanB: { type: String, default: null },
  imgPilihanB: { type: String, default: null },
  pilihanC: { type: String, default: null },
  imgPilihanC: { type: String, default: null },
  pilihanD: { type: String, default: null },
  imgPilihanD: { type: String, default: null },
});
const Soals = mongoose.model("soals", SoalsSchema);

const SchedulersSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kode_banksoal: { type: String, required: true },
  kode: { type: String, required: true },
  ruangan: { type: String, required: true },
  tgl_mulai: { type: Number, required: true },
  tgl_selesai: { type: Number, required: true },
  durasi: { type: Number, required: true },
  status: { type: Number, enum: [0, 1, 2], default: 0 },
  file_daftar_hadir: { type: String, required: false, default: null },
});
const Schedulers = mongoose.model("schedulers", SchedulersSchema);

const ScoresSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  id_schedule: { type: Schema.Types.ObjectId, required: true },
  nisn: { type: String, required: true },
  kode_ujian: { type: String, required: true },
  mapel: { type: String, required: true },
  nilai: { type: Number, required: true },
});
const Scores = mongoose.model("scores", ScoresSchema);

const AnnouncementSchema = new mongoose.Schema({
  utk: { type: String, required: true },
  judul: { type: String, required: true },
  isi: { type: String, required: true },
  createdAt: { type: Number, default: Date.now() },
});
const Announcement = mongoose.model("announcements", AnnouncementSchema);

const ExamCardsSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  kelas: { type: String, required: true },
  kode: { type: String, required: true },
});
const ExamCards = mongoose.model("examcards", ExamCardsSchema);

const NewsEventsSchema = new mongoose.Schema({
  catatan: { type: String, required: true },
  file: { type: String, required: true, default: null },
});
const NewsEvents = mongoose.model("newsevents", NewsEventsSchema);
const NewsEventFilesSchema = new mongoose.Schema({
  id_sekolah: { type: Schema.Types.ObjectId, required: true },
  id_newsevent: { type: Schema.Types.ObjectId, required: true },
  nama: { type: String, required: true, default: null },
  file: { type: String, required: true, default: null },
});
const NewsEventFiles = mongoose.model("newseventfiles", NewsEventFilesSchema);

module.exports.Dinas = Dinas;
module.exports.Schools = Schools;
module.exports.Students = Students;
module.exports.Classes = Classes;
module.exports.Rooms = Rooms;
module.exports.Mapels = Mapels;
module.exports.Materials = Materials;
module.exports.BankSoals = BankSoals;
module.exports.SupportFiles = SupportFiles;
module.exports.Soals = Soals;
module.exports.Schedulers = Schedulers;
module.exports.Scores = Scores;
module.exports.Announcement = Announcement;
module.exports.ExamCards = ExamCards;
module.exports.NewsEvents = NewsEvents;
module.exports.NewsEventFiles = NewsEventFiles;
